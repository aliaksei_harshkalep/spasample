﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TodoApplication.Models
{
    public enum Status
    {
        InProgress,
        Done
    }
}