﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TodoApplication.Models
{
    public class TodoItemEditModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Status Status { get; set; }
    }
}