﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TodoApplication.Models
{
    public class TodoItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Finished { get; set; }
        public Status Status { get; set; }
    }
}