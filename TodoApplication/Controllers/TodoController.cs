﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TodoApplication.Models;

namespace TodoApplication.Controllers
{
    public class TodoController : ApiController
    {
        private readonly TodoContext _context;

        public TodoController()
        {
            _context = new TodoContext();
        }

        // GET: api/Todo
        public async Task<IEnumerable<TodoItem>> Get(Status? status)
        {
            if (status.HasValue) return await _context.TodoItems.Where(t => t.Status == status).ToListAsync();

            return await _context.TodoItems.ToListAsync();
        }

        // GET: api/Todo/5
        public async Task<TodoItem> Get(int id)
        {
            return await _context.TodoItems.FindAsync(id);
        }

        // POST: api/Todo
        public async Task<IHttpActionResult> Post([FromBody]TodoItemEditModel item)
        {
            _context.TodoItems.Add(new TodoItem
            {
                Title = item.Title,
                Description = item.Description,
                Status = item.Status,
                Created = DateTime.Now
            });
            await _context.SaveChangesAsync();

            return Ok(true);
        }

        // PUT: api/Todo/5
        public async Task<IHttpActionResult> Put(int id, [FromBody]TodoItemEditModel item)
        {
            var editingItem = await _context.TodoItems.FindAsync(id);
            if (editingItem == null) return NotFound();

            editingItem.Title = item.Title;
            editingItem.Description = item.Description;
            editingItem.Status = item.Status;

            await _context.SaveChangesAsync();

            return Ok(true);
        }

        // DELETE: api/Todo/5
        public async Task<IHttpActionResult> Delete(int id)
        {
            var item = await _context.TodoItems.FindAsync(id);
            if (item == null) return NotFound();

            _context.TodoItems.Remove(item);
            await _context.SaveChangesAsync();

            return Ok(true);
        }
    }
}
