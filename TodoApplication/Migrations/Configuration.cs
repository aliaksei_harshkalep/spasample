namespace TodoApplication.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TodoApplication.Models.TodoContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TodoApplication.Models.TodoContext context)
        {
            //  This method will be called after migrating to the latest version.

            context.TodoItems.AddOrUpdate(new[]
            {
                new TodoItem { Id = 1, Title = "Shopping", Description = "To buy bread, milk and coffe.", Created = DateTime.Now, Status = Status.InProgress },
                new TodoItem { Id = 2, Title = "Working", Description = "Create greatest app.", Created = DateTime.Now, Finished = DateTime.Now, Status = Status.Done },
                new TodoItem { Id = 3, Title = "Hobby", Description = "Play board games.", Created = DateTime.Now, Status = Status.InProgress },
            });
        }
    }
}
