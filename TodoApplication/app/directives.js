﻿angular.module('todo.directives', ['ui.bootstrap'])
    .directive('todoConfirm', ['$uibModal', function ($uibModal) {
        return {
            restrict: 'A',
            scope: {
                title: '@confirmTitle',
                message: '@confirmMessage',
                callback: '&confirmCallback',
            },
            link: function (scope, element, attrs) {
                element.bind('click', function () {
                    var modalInstance = $uibModal.open({
                        templateUrl: '/app/confirm.html',
                        controller: function ($uibModalInstance) {
                            var self = this;

                            self.title = scope.title;
                            self.message = scope.message;
                            self.ok = function () {
                                var result = scope.callback()();

                                if (result) {
                                    result.then(function () {
                                        $uibModalInstance.close();
                                    })
                                }
                            };

                            self.cancel = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        },
                        controllerAs: 'confirmController'
                    });
                });
            }
        }
    }]);