﻿angular.module('todo.resources', ['ngResource'])
    .factory('Todo', ['$resource', function ($resource) {
        return $resource('api/todo/:id', {id: '@id'}, {
            'update': { method:'PUT' }
        });
    }]);