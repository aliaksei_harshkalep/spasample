﻿angular.module('todo', ['ui.router', 'ui.bootstrap', 'todo.resources', 'todo.directives'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise("/");

        $stateProvider.state('todo', {
            url: "",
            abstract: true,
            views: {
                '': {
                    templateUrl: '/app/app.html',
                },
                'header@todo': {
                    templateUrl: '/app/header.html',
                    controller: 'HeaderController',
                    controllerAs: 'headerController',
                },
                'footer@todo': {
                    templateUrl: '/app/footer.html',
                    controller: 'FooterController',
                    controllerAs: 'footerController'
                }
            }
        }).state('todo.index', {
            resolve: {
                Todo: 'Todo',                
                todos: function ($stateParams, Todo) {
                    return Todo.query({status: $stateParams.status});
                }
            },
            url: "/:status",
            templateUrl: '/app/todo.html',
            controller: 'TodoController',
            controllerAs: 'todoController',
        });

        //$locationProvider.html5Mode({
        //    enabled: true,
        //    requireBase: false
        //});

    }).controller('HeaderController', ['$scope', '$stateParams', function ($scope, $stateParams) {
        var self = this;

        self.status = $stateParams.status || null;

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams) {
            self.status = toParams.status || null;
        });

    }]).controller('FooterController', [function () {

    }]).controller('TodoController', ['$stateParams', '$uibModal', 'Todo', 'todos', function ($stateParams, $uibModal, Todo, todos) {
        var self = this;

        self.todos = todos;

        self.getTodos = function () {
            return self.todos;
        };

        self.add = function () {
            showEditModal();
        };

        self.edit = function (todo) {
            showEditModal(todo);
        };

        self.remove = function (todo) {
            return function () {
                return Todo.remove({ id: todo.id }).$promise.then(function () {
                    refreshTodos();
                });
            };
        };

        function showEditModal(todo) {
            var modalInstance = $uibModal.open({
                templateUrl: '/app/editTodo.html',
                controller: 'EditTodoController',
                controllerAs: 'editTodoController',
                resolve: {
                    editingTodo: todo
                }
            });

            modalInstance.result.then(function () {
                refreshTodos();
            });
        }

        function refreshTodos() {
            return Todo.query({status: $stateParams.status}).$promise.then(function (data) {
                self.todos = data;
            });
        }

    }]).controller('EditTodoController', ['$uibModalInstance', 'Todo', 'editingTodo', function ($uibModalInstance, Todo, editingTodo) {
        var self = this;

        self.isEditing = editingTodo ? editingTodo.id : null;
        self.title = editingTodo ? editingTodo.title : '';
        self.description = editingTodo ? editingTodo.description : '';
        self.status = editingTodo ? editingTodo.status.toString() : '0';

        self.ok = function () {
            var promise;
            if (self.isEditing) {
                promise = Todo.update({
                    id: editingTodo.id,
                    title: self.title,
                    description: self.description,
                    status: self.status
                }).$promise;
            } else {
                promise = Todo.save({
                    title: self.title,
                    description: self.description,
                    status: self.status
                }).$promise;
            }

            promise.then(function () {
                $uibModalInstance.close(self.item);
            });
        };

        self.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);